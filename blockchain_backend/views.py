from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
# Create your views here.
import sqlite3



def dict_factory(cursor, row):
   d = {}
   for idx, col in enumerate(cursor.description):
       d[col[0]] = row[idx]
   return d

def get_all_data():
    connection = sqlite3.connect("../../ticker.db_dec20_2017")
    connection.row_factory = dict_factory
    cursor = connection.cursor()
    
    results= cursor.execute("select currency_id, price_usd, market_cap_usd,percent_change_24h,percent_change_7d,rank,available_supply,volume_usd_24h from ticker_info limit 50")
    results = cursor.fetchall()
    return results

def get_currency(request):
    
    results = get_all_data()
    
    print("abc")
    return JsonResponse({"results":results})


